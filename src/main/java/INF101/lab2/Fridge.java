package INF101.lab2;


import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
    ArrayList<FridgeItem> fridgeItems;
    int max_size = 20;

    public Fridge() {
        fridgeItems = new ArrayList<FridgeItem>();
    }

    @Override
    public int nItemsInFridge() {
        return fridgeItems.size();
    }

    @Override
    public int totalSize() {
        return max_size;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (nItemsInFridge() < totalSize()) {
            fridgeItems.add(item);
            return true;
        }
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (!(fridgeItems.contains(item))) {
            throw new NoSuchElementException();
        } else {
            fridgeItems.remove(item);
        }
    }

    @Override
    public void emptyFridge() {
        fridgeItems.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expiredItem = new ArrayList<>();
        for (FridgeItem item : fridgeItems) {
            if (item.hasExpired()) {
                expiredItem.add(item);
            }

        }
        fridgeItems.removeAll(expiredItem);

        return expiredItem;
    }
}







